package com.vsevolod.soroka.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.vsevolod.soroka.logic.GoToIngredientWorkerLogic;
import com.vsevolod.soroka.logic.GoToPizzaWorkerLogic;
import com.vsevolod.soroka.manager.ConfigurationManager;

public class GoToPizzaWorker implements ICommand  {

	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response) {
		HttpSession session = request.getSession(true);
		session.setAttribute("AllPizzas", GoToPizzaWorkerLogic.getAllPizzas());
		return ConfigurationManager.getInstance().getProperty(ConfigurationManager.PIZZA_WORKER);
	}

}
