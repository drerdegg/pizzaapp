package com.vsevolod.soroka.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

//import org.apache.log4j.Logger;

//import ua.bionic.hotelreserve.bean.Client;
//import ua.bionic.hotelreserve.dao.factory.MysqlDaoFactory;
//import ua.bionic.hotelreserve.logic.LoadLobbyDataLogic;
//import ua.bionic.hotelreserve.logic.LoadManagePanelLogic;

import com.vsevolod.soroka.entity.Login;
import com.vsevolod.soroka.logic.LoginLogic;

import com.vsevolod.soroka.manager.ConfigurationManager;

/**
 * Command for user login
 *
 * @version 1.0
 * @author saresh
 */
public  class LoginCommand implements ICommand {
    
    //private static Logger logger = Logger.getLogger(LoginCommand.class);
    
    @Override
    public String execute(HttpServletRequest request,
            HttpServletResponse response) {
        //int userId;
        String page = null;

        //get login and password from form
        String loginString = request.getParameter("login");
        String passwordString = request.getParameter("password");
        Login login = LoginLogic.getLoginFromDB(loginString);
        //check for such user in db
        if (LoginLogic.checkLogin(loginString, passwordString)) {
            HttpSession session = request.getSession(true);
                        
            if (LoginLogic.isClient(loginString)) {
                session.setAttribute("login", login);
                session.setAttribute("loginName", login.getLoginName());
                
                //LoadLobbyDataLogic.updateRequestsLists(clientId, session);
                
                //logger.info("User(id = " + login.getId() + " ) logged in [Client]");
                
                page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.SUCCESS_LOGIN_PAGE);
            } 
            else {
            	 page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ADMIN_PAGE);
//                LoadManagePanelLogic.loadAllPendingRequests(session);
//                userId = LoginLogic.getUserIdByLogin(login);
//                session.setAttribute("userId", userId);
//                page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.MANAGE_PAGE);
//                
//                logger.info("User(id = " + userId + " ) logged in [Admin]");
            }
        } else {
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.LOGIN_PAGE);
        }
        
        return page;
    }
    
}
