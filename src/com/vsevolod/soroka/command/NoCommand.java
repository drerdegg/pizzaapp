package com.vsevolod.soroka.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.vsevolod.soroka.manager.ConfigurationManager;

/**
 * Command if other commands wasn't used
 * 
 * @version 1.0
 * @author saresh
 */
public class NoCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String page;

        HttpSession session = request.getSession(true);

        if (session.getAttribute("client") != null) {
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.LOBBY_PAGE);
        } else {
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.MAIN_PAGE);
        }

        return page;
    }

}
