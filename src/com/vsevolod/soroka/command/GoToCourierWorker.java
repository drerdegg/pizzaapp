package com.vsevolod.soroka.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.vsevolod.soroka.logic.GoToCourierWorkerLogic;
import com.vsevolod.soroka.manager.ConfigurationManager;

public class GoToCourierWorker implements ICommand {

	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response) {
		HttpSession session = request.getSession(true);
		session.setAttribute("AllCouriers", GoToCourierWorkerLogic.getAllCouriers());
		return ConfigurationManager.getInstance().getProperty(ConfigurationManager.COURIER_WORKER);
	}

}
