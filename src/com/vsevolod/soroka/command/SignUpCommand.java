package com.vsevolod.soroka.command;


import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.vsevolod.soroka.dao.factory.Factory;
import com.vsevolod.soroka.entity.Login;
import com.vsevolod.soroka.logic.SignUpLogic;
import com.vsevolod.soroka.manager.ConfigurationManager;

/**
 * Command for sign up. Check for avaliability of login, validates fields and if
 * unique login - creates user, if not unique or invalid fields - returns user
 * to sign up page
 *
 * @version 1.0
 * @author saresh
 */
public class SignUpCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String page;

        String login = request.getParameter("login");
        String password = request.getParameter("password");
        String fName = request.getParameter("firstName");
        String lName = request.getParameter("lastName");
        String email = request.getParameter("email");
        String phone = request.getParameter("phoneNumber");
        String adress = request.getParameter("adress");

        //Create to DB new Client & user
        if (!SignUpLogic.isDuplicateUser(login)) {
            if (!SignUpLogic.isValidEmail(email) || !SignUpLogic.isValidPhoneNumber(phone)
                    || login.isEmpty() || password.isEmpty() || fName.isEmpty() || lName.isEmpty()) {
                page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.REGISTRATE_PAGE);
            } else {
                SignUpLogic.addLoginToDb(login,password,fName, lName, email, phone,adress);
                //SignUpLogic.addUserToDb(login, password, User.UserType.CLIENT, clientId);
                page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.SUCCESS_LOGIN_PAGE);
            }
        } else {
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.REGISTRATE_PAGE);
        }

        return page;
    }

}
