package com.vsevolod.soroka.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.vsevolod.soroka.logic.GoToCourierWorkerLogic;
import com.vsevolod.soroka.logic.GoToIngredientWorkerLogic;
import com.vsevolod.soroka.manager.ConfigurationManager;

public class GoToIngredientWorker implements ICommand {

	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response) {
		HttpSession session = request.getSession(true);
		session.setAttribute("AllIngredients", GoToIngredientWorkerLogic.getAllIngredients());
		return ConfigurationManager.getInstance().getProperty(ConfigurationManager.INGREDIENT_WORKER);
	}

}
