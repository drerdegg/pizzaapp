package com.vsevolod.soroka.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sun.org.apache.bcel.internal.generic.FMUL;
import com.sun.org.apache.bcel.internal.generic.RETURN;
import com.vsevolod.soroka.entity.Courier;
import com.vsevolod.soroka.logic.AddCourierLogic;
import com.vsevolod.soroka.manager.ConfigurationManager;

public class AddCourier implements ICommand {
	String page;
	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response) {
        String fName = request.getParameter("firstName");
        String lName = request.getParameter("lastName");
        String phoneNumber = request.getParameter("phoneNumber");
        
        if(!fName.isEmpty() && !lName.isEmpty() && AddCourierLogic.isValidPhoneNumber(phoneNumber))
        {
        	AddCourierLogic.addCourierToDb(fName,lName,phoneNumber);
        	
        }
        page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.COURIER_WORKER);
        return page;
	}
	
}
