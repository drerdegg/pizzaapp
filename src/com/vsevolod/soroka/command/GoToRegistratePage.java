package com.vsevolod.soroka.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.vsevolod.soroka.manager.ConfigurationManager;

public class GoToRegistratePage implements ICommand {

	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response) {
		return ConfigurationManager.getInstance().getProperty(ConfigurationManager.REGISTRATE_PAGE);
	}

}
