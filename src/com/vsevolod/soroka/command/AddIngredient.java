package com.vsevolod.soroka.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.vsevolod.soroka.logic.AddCourierLogic;
import com.vsevolod.soroka.logic.AddIngredientLogic;
import com.vsevolod.soroka.manager.ConfigurationManager;

public class AddIngredient implements ICommand {
	String page;
	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response) {
        String fName = request.getParameter("ingredientName");
        String lName = request.getParameter("ingredientType");
        
        if(!fName.isEmpty() && !lName.isEmpty() )
        {
        	
        	AddIngredientLogic.addIngredientToDb(fName, lName);
        	
        }
        page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.INGREDIENT_WORKER);
        return page;
	}
}
