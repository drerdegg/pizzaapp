package com.vsevolod.soroka.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Command interface
 *
 * @version 1.0
 * @author saresh
 */
public interface ICommand {

    /**
     * Execute command, work with request and response parameter and return to
     * controller path to result page
     *
     * @param request request from servlet
     * @param response response from servlet
     * @return path to result page
     */
    String execute(HttpServletRequest request, HttpServletResponse response);
}
