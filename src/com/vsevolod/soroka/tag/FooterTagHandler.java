package com.vsevolod.soroka.tag;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import static javax.servlet.jsp.tagext.Tag.SKIP_BODY;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * Tag for inserting footer on page
 * 
 * @version 1.0
 * @author saresh
 */
public class FooterTagHandler extends TagSupport {

    @Override
    public int doStartTag() throws JspException {

        JspWriter out = pageContext.getOut();
        String footerText = "<div class=\"footer\"> \n"
                + "� 2014 Vsevolod Soroka"
                + "</div>";
        try {
            out.write(footerText);
        } catch (IOException ex) {
            Logger.getLogger(FooterTagHandler.class.getName()).log(Level.SEVERE, null, ex);
        }

        return SKIP_BODY;
    }

}
