package com.vsevolod.soroka.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "ingredientName")
public class IngredientName implements java.io.Serializable  {

	private int ingredientNameID;
	private String ingredientName;
	private String ingredientType;
	private Set<Ingredient> ingredients = new HashSet<Ingredient>(0);
	
	public IngredientName(){}
	public IngredientName(String ingredientName, String ingredientType) {
		this.ingredientName = ingredientName;
		this.ingredientType = ingredientType;
	}
	@Id
	@GeneratedValue
	@Column(name = "ingredientNameID", unique = true, nullable = false)
	public int getIngredientNameID() {
		return ingredientNameID;
	}
	public void setIngredientNameID(int ingredientNameID) {
		this.ingredientNameID = ingredientNameID;
	}
	
	@Column(name = "ingredientName")
	public String getIngredientName() {
		return ingredientName;
	}
	public void setIngredientName(String ingredientName) {
		this.ingredientName = ingredientName;
	}
	
	@Column(name = "ingredientType")
	public String getIngredientType() {
		return ingredientType;
	}
	
	public void setIngredientType(String ingredientType) {
		this.ingredientType = ingredientType;
	}
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "ingredientName")
	public Set<Ingredient> getIngredients() {
		return ingredients;
	}
	public void setIngredients(Set<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}
	
	
	
	
}
