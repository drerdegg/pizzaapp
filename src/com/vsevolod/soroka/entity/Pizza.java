package com.vsevolod.soroka.entity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.CascadeType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
 

@Entity
@Table(name="Pizza")
public  class Pizza{

	private int pizzaID;
	private String pizzaName;
	private int pizzaSize;
	private Set<Ingredient> ingredients = new HashSet<Ingredient>(0);
	private Set<Order> orders = new HashSet<Order>(0);
	
	public Pizza(){}
	public Pizza(String pizzaName, int pizzaSize) {
		super();
		this.pizzaName = pizzaName;
		this.pizzaSize = pizzaSize;
	}
	

	
	@Id
	@GeneratedValue
	public int getPizzaID() {
		return pizzaID;
	}
	public void setPizzaID(int pizzaID) {
		this.pizzaID = pizzaID;
	}
	@Column(name = "pizzaName")
	public String getPizzaName() {
		return pizzaName;
	}

	public void setPizzaName(String pizzaName) {
		this.pizzaName = pizzaName;
	}

	@Column(name = "pizzaSize")
	public int getPizzaSize() {
		return pizzaSize;
	}
	public void setPizzaSize(int pizzaSize) {
		this.pizzaSize = pizzaSize;
	}
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "pizza_ingredient", joinColumns = { 
			@JoinColumn(name = "id", nullable = false, updatable = false) }, 
			inverseJoinColumns = { @JoinColumn(name = "ingredientsID", 
					nullable = false, updatable = false) })
	public Set<Ingredient> getIngredients() {
		return ingredients;
	}
	public void setIngredients(Set<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}
	
	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "pizzas")
	public Set<Order> getOrders() {
		return orders;
	}
	public void setOrders(Set<Order> orders) {
		this.orders = orders;
	}
	
	
	

	
	
	

	
}
