package com.vsevolod.soroka.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="courier")
public class Courier {

	
	private int courierID;
	private String firstName;
	private String lastName;
	private String telNumber;
	private Set<Order> orders = new HashSet<Order>(0);
	
	public Courier(){};
	public Courier( String firstName, String lastName, String telNumber) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.telNumber = telNumber;
	}
	
	@Id @GeneratedValue
	@Column(name = "courierID")
	public int getId() {
		return courierID;
	}
	public void setId(int id) {
		this.courierID = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getTelNumber() {
		return telNumber;
	}
	public void setTelNumber(String telNumber) {
		this.telNumber = telNumber;
	}
	
	public String toString()
	{
		return "|" + firstName + " | " +lastName + " | " + telNumber+"|";
	}
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "courier")
	public Set<Order> getOrders() {
		return orders;
	}
	public void setOrders(Set<Order> orders) {
		this.orders = orders;
	}
	
	
}
