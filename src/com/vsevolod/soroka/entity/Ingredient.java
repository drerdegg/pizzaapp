package com.vsevolod.soroka.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import javax.persistence.ManyToMany;
@Entity
@Table(name="ingredients")
public class Ingredient implements java.io.Serializable{

	
	private int ingredientsID;
	
	private IngredientName ingredientName;

	private float weight;
	
	private Set<Pizza> pizzas = new HashSet<Pizza>(0);
	
	public Ingredient(){}

	public Ingredient(IngredientName ingredientsNameID, float weight) {
		this.ingredientName = ingredientsNameID;
		this.weight = weight;
	}

	@Id
	@GeneratedValue
	public int getIngredientsID() {
		return ingredientsID;
	}

	public void setIngredientsID(int ingredientsID) {
		this.ingredientsID = ingredientsID;
	}


	

	public float getWeight() {
		return weight;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ingredientName", nullable = false)
	public IngredientName getIngredientName() {
		return ingredientName;
	}

	public void setIngredientName(IngredientName ingredientName) {
		this.ingredientName = ingredientName;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "ingredients")
	public Set<Pizza> getPizzas() {
		return pizzas;
	}

	public void setPizzas(Set<Pizza> pizzas) {
		this.pizzas = pizzas;
	}
	
	public String toString()
	{
		return "|"+ingredientName.getIngredientName() + "|" + weight;
	}
	
	

}
