package com.vsevolod.soroka.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

@Entity
@Table(name="login")
public  class Login {


	private int id;
	private String loginName;
	private String password;
	private String email;
	private boolean blackList;
	private Customer customer;
	
	
	public Login(){}
	public Login( String loginName, String firstName, String secondName,
			String password, String email, String phoneNumber, String adress) {
		this.customer = new Customer();
		this.loginName = loginName;
		this.customer.setFirsName(firstName);
		this.customer.setLastName(secondName);
		this.password = password;
		this.email = email;
		this.customer.setPhoneNumber(phoneNumber);
		this.customer.setAdress(adress);
		this.blackList = false;
	}

	@Column(name = "loginName")
	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	@Id @GeneratedValue
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "password")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	
	@Column(name = "blackList")
	public boolean isBlackList() {
		return blackList;
	}

	public void setBlackList(boolean blackList) {
		this.blackList = blackList;
	}
	
	@OneToOne(cascade = CascadeType.ALL)
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	
	
}