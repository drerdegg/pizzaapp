package com.vsevolod.soroka.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="OrderTable")
public class Order {

	private int orderID;
	private Customer customer;
	private Courier courier;
	private Set<Pizza> pizzas = new HashSet<Pizza>(0);
	public Order(){}
	
	
	public Order(Customer customer, Courier courier) {
		this.customer = customer;
		this.courier = courier;
	}

	@Id @GeneratedValue
	public int getOrderID() {
		return orderID;
	}


	public void setOrderID(int orderID) {
		this.orderID = orderID;
	}


	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "customerID", nullable = false)
	public Customer getCustomer() {
		return customer;
	}
	


	public void setCustomer(Customer customer) {
		this.customer = customer;
	}


	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "courierID", nullable = false)
	public Courier getCourier() {
		return courier;
	}
	public void setCourier(Courier courier) {
		this.courier = courier;
	}


	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "pizza_order", joinColumns = { 
			@JoinColumn(name = "orderID", nullable = false, updatable = false) }, 
			inverseJoinColumns = { @JoinColumn(name = "pizzaID", 
					nullable = false, updatable = false) })
	public Set<Pizza> getPizzas() {
		return pizzas;
	}


	public void setPizzas(Set<Pizza> pizzas) {
		this.pizzas = pizzas;
	}
	
	
}
