package com.vsevolod.soroka.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "customer")
public class Customer {

	private int customerID;
	private String firsName;
	private String lastName;
	private String phoneNumber;
	private String Adress;
	private Set<Order> orders = new HashSet<Order>(0);
	
	public Customer(){}
	
	public Customer( String firsName, String lastName, String phoneNumber,
			String adress) {
		this.firsName = firsName;
		this.lastName = lastName;
		this.phoneNumber = phoneNumber;
		Adress = adress;
	}

	
//	@PrimaryKeyJoinColumn
//	@OneToOne(fetch = FetchType.LAZY)
//	public Login getLogin() {
//		return login;
//	}
//
//	public void setLogin(Login login) {
//		this.login = login;
//	}
	
	@Id @GeneratedValue
	public int getCustomerId() {
		return customerID;
	}
	public void setCustomerId(int id) {
		this.customerID = id;
	}
	public String getFirsName() {
		return firsName;
	}
	public void setFirsName(String firsName) {
		this.firsName = firsName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAdress() {
		return Adress;
	}
	public void setAdress(String adress) {
		Adress = adress;
	} 
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "customer")
	public Set<Order> getOrders() {
		return orders;
	}
	public void setOrders(Set<Order> orders) {
		this.orders = orders;
	}

}
