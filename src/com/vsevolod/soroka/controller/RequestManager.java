package com.vsevolod.soroka.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
/*import ua.bionic.hotelreserve.command.ChangeLocaleCommand;
import ua.bionic.hotelreserve.command.CreateRequestCommand;
import ua.bionic.hotelreserve.command.EnterFromMainPageCommand;*/










import com.vsevolod.soroka.command.AddCourier;
import com.vsevolod.soroka.command.AddIngredient;
import com.vsevolod.soroka.command.GoToCourierWorker;
import com.vsevolod.soroka.command.GoToIngredientWorker;
import com.vsevolod.soroka.command.GoToLoginPage;
import com.vsevolod.soroka.command.GoToLoginWorker;
import com.vsevolod.soroka.command.GoToPizzaWorker;
import com.vsevolod.soroka.command.GoToRegistratePage;
import com.vsevolod.soroka.command.ICommand;
import com.vsevolod.soroka.command.LoginCommand;
//import ua.bionic.hotelreserve.command.LogOutCommand;
//import ua.bionic.hotelreserve.command.MarkRequestCanceledCommand;
import com.vsevolod.soroka.command.NoCommand;
//import ua.bionic.hotelreserve.command.OpenPayPageCommand;
//import ua.bionic.hotelreserve.command.PayForRequestCommand;
//import ua.bionic.hotelreserve.command.OpenSelectRoomPageCommand;
//import ua.bionic.hotelreserve.command.SelectRoomForRequestCommand;
import com.vsevolod.soroka.command.SignUpCommand;

/**
 * Manager, which indicates controller which command to use 
 * 
 * @version 1.0
 * @author saresh
 */
public class RequestManager {

    private static RequestManager instance = null;
    private HashMap<String, ICommand> commands = new HashMap<>();

    private RequestManager() {
          commands.put("login", new  LoginCommand());
    	  commands.put("goToRegistratePage", new  GoToRegistratePage());
    	  commands.put("goToLoginPage", new  GoToLoginPage());
    	  commands.put("goToPizzaWorker", new GoToPizzaWorker());
    	  commands.put("goToLoginWorker", new GoToLoginWorker());
    	  commands.put("goToIngredientWorker", new GoToIngredientWorker());
    	  commands.put("goToCourierWorker", new GoToCourierWorker());
    	  commands.put("addCourier", new AddCourier());
    	  commands.put("addIngredient", new AddIngredient());
    	  
//        commands.put("createRequest", new CreateRequestCommand());
//        commands.put("openPayPage", new OpenPayPageCommand());
//        commands.put("payForRequest", new PayForRequestCommand());
//        commands.put("openSelectRoomPage", new OpenSelectRoomPageCommand());
//        commands.put("selectRoom", new SelectRoomForRequestCommand());
//        commands.put("enterFromMainPage", new EnterFromMainPageCommand());
//        commands.put("logOut", new LogOutCommand());
          commands.put("signUp", new SignUpCommand());
//        commands.put("changeLocale", new ChangeLocaleCommand());
//        commands.put("markCanceled", new MarkRequestCanceledCommand());

    }

    public ICommand getCommand(HttpServletRequest request) {
        String action = request.getParameter("command");
        ICommand command = commands.get(action);
        if (command == null) {
            command = new NoCommand();
        }

        return command;
    }

    public static RequestManager getInstance() {
        if (instance == null) {
            instance = new RequestManager();
        }

        return instance;
    }
    
    @Override
    public Object clone() {
        throw new UnsupportedOperationException();
    }
}
