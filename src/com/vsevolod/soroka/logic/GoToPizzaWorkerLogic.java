package com.vsevolod.soroka.logic;

import java.sql.SQLException;
import java.util.ArrayList;

import com.vsevolod.soroka.dao.factory.Factory;
import com.vsevolod.soroka.entity.Login;
import com.vsevolod.soroka.entity.Pizza;

public class GoToPizzaWorkerLogic {

	public static ArrayList<Pizza> getAllPizzas()
	{
		
		try {
			return Factory.getInstance().getPizzaDAO().getAllPizza();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}
