package com.vsevolod.soroka.logic;

import java.sql.SQLException;
import java.util.ArrayList;

import com.vsevolod.soroka.dao.factory.Factory;
import com.vsevolod.soroka.entity.Courier;

public class GoToCourierWorkerLogic {

	public static ArrayList<Courier> getAllCouriers()
	{
		
		try {
			return Factory.getInstance().getCourierDAO().getAllCourier();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}
