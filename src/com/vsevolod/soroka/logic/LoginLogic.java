package com.vsevolod.soroka.logic;

import java.sql.SQLException;

import com.vsevolod.soroka.dao.factory.Factory;
import com.vsevolod.soroka.entity.Login;

public class LoginLogic {
	 /**
     * Checks if such user exists
     * 
     * @param login user login
     * @param password user password
     * @return true if user exists, false if no such user
     */
    public static boolean checkLogin(String loginString, String passwordString) {
       Login login = getLoginFromDB(loginString);
       return ( login != null && login.getPassword().equals(passwordString)) || (loginString.equals("admin") && loginString.equals("admin"));
    }

    public static Login getLoginFromDB(String login)
    {
    	Login l = null;
    	try {
			l = Factory.getInstance().getLoginDAO().getLoginByLoginsName(login);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return l;
    }
    /**
     * Checks if user is a client
     * 
     * @param login user login
     * @return true if user is client, false if not
     */
    public static boolean isClient(String loginString) {
        return  !loginString.equals("admin");
    }

//    /**
//     * Checks if user is an admin
//     * 
//     * @param login user login
//     * @return true if user is admin, false if not
//     */
//    public static boolean isAdmin(String login) {
//        boolean isAdmin;
//        User user = MysqlDaoFactory.getDaoUser().findUserByLogin(login);
//
//        if (user.getUserType() == User.UserType.ADMIN) {
//            isAdmin = true;
//        } else {
//            isAdmin = false;
//        }
//
//        return isAdmin;
//    }
//
//    /**
//     * Gets user's client id
//     * 
//     * @param login user login
//     * @return user's client id
//     */
//    public static int getUsersClientId(String login) {
//        int clientId;
//        User user = MysqlDaoFactory.getDaoUser().findUserByLogin(login);
//
//        clientId = user.getClientId();
//
//        return clientId;
//    }
//
//    /**
//     * Gets user id by login
//     * 
//     * @param login user login
//     * @return user id
//     */
//    public static int getUserIdByLogin(String login) {
//        int userId;
//
//        User user = MysqlDaoFactory.getDaoUser().findUserByLogin(login);
//        userId = user.getId();
//
//        return userId;
//    }
}