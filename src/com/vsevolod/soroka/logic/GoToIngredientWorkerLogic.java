package com.vsevolod.soroka.logic;

import java.sql.SQLException;
import java.util.ArrayList;

import com.vsevolod.soroka.dao.factory.Factory;
import com.vsevolod.soroka.entity.Ingredient;

public class GoToIngredientWorkerLogic {
	public static ArrayList<Ingredient> getAllIngredients()
	{
		
		try {
			return Factory.getInstance().getIngredientDAO().getAllIngredient();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}
