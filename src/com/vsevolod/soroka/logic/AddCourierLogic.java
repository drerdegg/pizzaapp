package com.vsevolod.soroka.logic;

import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.vsevolod.soroka.dao.factory.Factory;
import com.vsevolod.soroka.entity.Courier;
import com.vsevolod.soroka.entity.Login;

public class AddCourierLogic {

	  public static boolean isValidPhoneNumber(String phoneNumber) {
	        String phonePattern = "^[0-9]{11}";
	        Pattern pattern = Pattern.compile(phonePattern);
	        Matcher matcher = pattern.matcher(phoneNumber);

	        return matcher.matches();
	    }
	  
		public static void addCourierToDb( String fName,String lName, String phoneNumber) {
		   	Courier courier = new Courier(fName, lName, phoneNumber);
	    	try {
				Factory.getInstance().getCourierDAO().addCourier(courier);
			} catch (SQLException e) {
				e.printStackTrace();
			}
	        //MysqlDaoFactory.getDaoClient().addClientToDb(login);
	        //int clientID = MysqlDaoFactory.getDaoClient().getLastClientAddedId();

	        //return clientID;
		}
}
