package com.vsevolod.soroka.logic;

import java.sql.SQLException;

import com.vsevolod.soroka.dao.factory.Factory;
import com.vsevolod.soroka.entity.Courier;
import com.vsevolod.soroka.entity.Ingredient;
import com.vsevolod.soroka.entity.IngredientName;

public class AddIngredientLogic {
	public static void addIngredientToDb( String ingredientName,String ingredientType) {
	   	IngredientName ingredient = new IngredientName(ingredientName, ingredientType); 
    	try {
			Factory.getInstance().getIngredientNameDAO().addIngredientName(ingredient);
		} catch (SQLException e) {
			e.printStackTrace();
		}
        //MysqlDaoFactory.getDaoClient().addClientToDb(login);
        //int clientID = MysqlDaoFactory.getDaoClient().getLastClientAddedId();

        //return clientID;
	}
}
