package com.vsevolod.soroka.logic;


import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.vsevolod.soroka.dao.factory.Factory;
import com.vsevolod.soroka.entity.Courier;
import com.vsevolod.soroka.entity.Customer;
import com.vsevolod.soroka.entity.Login;
//import com.vsevolod.soroka.entity.MysqlDaoFactory;

/**
 * Logic for sign up command
 * 
 * @version 1.0
 * @author saresh
 */
public class SignUpLogic {

    /**
     * Adds client to database
     * @param fName first name
     * @param lName last name 
     * @param email email
     * @param phone phone number
     * @return client's id from table
     */


    /**
     * Check if login is duplicate
     * 
     * @param login login to check
     * @return true if such user exists, false if login is unique
     */
    public static boolean isDuplicateUser(String login) {
    	return false;
        //return MysqlDaoFactory.getDaoUser().findUserByLogin(login) != null;
    }

    /**
     * Adds user to database
     * 
     * @param login user login
     * @param password user password
     * @param type user type
     * @param clientId user's client id
     */
//    public static void addUserToDb(String login, String password, User.UserType type, int clientId) {
//        User user = new User();
//        user.setLogin(login);
//        user.setPassword(password);
//        user.setUserType(type);
//        user.setClientId(clientId);
//
//        MysqlDaoFactory.getDaoUser().addUserToDb(user);
//    }

    /**
     * Checks if email is valid
     * 
     * @param email email to check
     * @return true if email is valid, false if not
     */
    public static boolean isValidEmail(String email) {
        String emailPattern
                = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(emailPattern);
        Matcher matcher = pattern.matcher(email);

        return matcher.matches();
    }

    /**
     * Checks if phone number is valid
     * 
     * @param phoneNumber number to check
     * @return true if phone number is valid, false if not
     */
    public static boolean isValidPhoneNumber(String phoneNumber) {
        String phonePattern = "^[0-9]{11}";
        Pattern pattern = Pattern.compile(phonePattern);
        Matcher matcher = pattern.matcher(phoneNumber);

        return matcher.matches();
    }
    
   

	public static void addLoginToDb(String loginName, String password, String fName,
			String lName, String email, String phone,String adress) {
		Login login = new Login(loginName,fName,lName,password,email,phone, adress);
    	try {
			Factory.getInstance().getLoginDAO().addLogin(login);
		} catch (SQLException e) {
			e.printStackTrace();
		}
        System.out.print("add login to DB");
        //MysqlDaoFactory.getDaoClient().addClientToDb(login);
        //int clientID = MysqlDaoFactory.getDaoClient().getLastClientAddedId();

        //return clientID;
	}
}
