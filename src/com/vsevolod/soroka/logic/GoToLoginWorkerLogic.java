package com.vsevolod.soroka.logic;

import java.sql.SQLException;
import java.util.ArrayList;

import com.vsevolod.soroka.dao.factory.Factory;
import com.vsevolod.soroka.entity.Courier;
import com.vsevolod.soroka.entity.Login;

public class GoToLoginWorkerLogic {

	public static ArrayList<Login> getAllLogins()
	{
		
		try {
			return Factory.getInstance().getLoginDAO().getAllLogin();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}
