package com.vsevolod.soroka.manager;


import java.util.ResourceBundle;

/**
 * Operates with pages adresses, holds pages constants
 * 
 * @version 1.0
 * @author saresh
 */
public class ConfigurationManager{

    private static ConfigurationManager instance;
    private ResourceBundle resourceBundle;

    private static final String BUNDLE_NAME = "com.vsevolod.soroka.manager.pages";

    public static final String ADMIN_PAGE = "ADMIN_PAGE";
    public static final String REGISTRATE_PAGE = "REGISTRATE_PAGE";
    public static final String MAIN_PAGE = "MAIN_PAGE";
    public static final String LOGIN_PAGE = "LOGIN_PAGE";
    public static final String SIGN_IN_PAGE = "SIGN_IN_PAGE";
    public static final String LOBBY_PAGE = "LOBBY_PAGE";
    public static final String MANAGE_PAGE = "MANAGE_PAGE";
    public static final String PAYMENT_PAGE = "PAYMENT_PAGE";
    public static final String SUCCESS_PAYMENT_PAGE = "SUCCESS_PAYMENT_PAGE";
    public static final String SELECT_ROOM_PAGE = "SELECT_ROOM_PAGE";
    public static final String SIGN_UP_PAGE = "SIGN_UP_PAGE";
    public static final String SUCCESS_SIGN_UP = "SUCCESS_SIGN_UP";
    public static final String SUCCESS_LOGIN_PAGE = "SUCCESS_LOGIN_PAGE";
	public static final String PIZZA_WORKER = "PIZZA_WORKER";
	public static final String LOGIN_WORKER = "LOGIN_WORKER";
	public static final String COURIER_WORKER = "COURIER_WORKER";
	public static final String INGREDIENT_WORKER = "INGREDIENT_WORKER";
    /**
     * @return instance if Configuration manager
     */
    public static ConfigurationManager getInstance() {
        if (instance == null) {
            instance = new ConfigurationManager();
            instance.resourceBundle = ResourceBundle.getBundle(BUNDLE_NAME);
        }
        return instance;
    }

    /**
     * Gets page path from resource bundle
     * 
     * @param key page name key
     * @return page path
     */
    public String getProperty(String key) {
        return (String) resourceBundle.getObject(key);
    }
    
    @Override
    public Object clone() {
        throw new UnsupportedOperationException();
    }

}
