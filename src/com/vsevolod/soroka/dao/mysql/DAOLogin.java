package com.vsevolod.soroka.dao.mysql;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.vsevolod.soroka.HibernateUtil;
import com.vsevolod.soroka.dao.interfaces.ILogin;
import com.vsevolod.soroka.entity.Login;

public class DAOLogin implements ILogin {

	@Override
	public void addLogin(Login login) throws SQLException {
		Session session = null;
	    try {
	      session = HibernateUtil.getSessionFactory().openSession();
	      session.beginTransaction();
	      session.save(login);
	      session.getTransaction().commit();
	    } catch (Exception e) {
	      System.out.print(e);//JOptionPane.showMessageDialog(null, e.getMessage(), ������� ��� �������, JOptionPane.OK_OPTION);
	    } finally {
	      if (session != null && session.isOpen()) {

	        session.close();
	      }
	    }
	}

	@Override
	public void updateLogin(Login login) throws SQLException {
		Session session = null;
	    try {
	      session = HibernateUtil.getSessionFactory().openSession();
	      session.beginTransaction();
	      session.update(login);
	      session.getTransaction().commit();
	    } catch (Exception e) {
	      //JOptionPane.showMessageDialog(null, e.getMessage(), ������� ��� �������, JOptionPane.OK_OPTION);
	    } finally {
	      if (session != null && session.isOpen()) {
	        session.close();
	      }
	    }
	}

	@Override
	public void deleteLogin(Login login) throws SQLException {
		Session session = null;
	    try {
	      session = HibernateUtil.getSessionFactory().openSession();
	      session.beginTransaction();
	      session.delete(login);
	      session.getTransaction().commit();
	    } catch (Exception e) {
	      //JOptionPane.showMessageDialog(null, e.getMessage(), ������� ��� ��������, JOptionPane.OK_OPTION);
	    } finally {
	      if (session != null && session.isOpen()) {
	        session.close();
	      }
	    }
	}

	@Override
	public Login getLoginByID(int loginID) throws SQLException {
		Session session = null;
	    Login login = null;
	    try {
	      session = HibernateUtil.getSessionFactory().openSession();
	      login = (Login) session.load(Login.class, loginID);
	    } catch (Exception e) {
	      //JOptionPane.showMessageDialog(null, e.getMessage(), ������� 'findById'�, JOptionPane.OK_OPTION);
	    } finally {
	      if (session != null && session.isOpen()) {
	        session.close();
	      }
	    }
	    return login;
	}

	@Override
	public ArrayList<Login> getAllLogin() throws SQLException {
		Session session = null;
	    List logins = new ArrayList<Login>();
	    try {
	      session = HibernateUtil.getSessionFactory().openSession();
	      logins = session.createCriteria(Login.class).list();
	    } catch (Exception e) {
	      //JOptionPane.showMessageDialog(null, e.getMessage(), ������� 'getAll'�, JOptionPane.OK_OPTION);
	    } finally {
	      if (session != null && session.isOpen()) {
	        session.close();
	      }
	    }
	    return (ArrayList<Login>)logins;
	}
	
	@Override
	  public Login getLoginByLoginsName(String loginName)
	  {
		    Session session = null;
		    Login login = null;
		    try {
		      session = HibernateUtil.getSessionFactory().openSession();
		      //login = (Login) session.load(Login.class, loginName);
		      Criteria criteria = session.createCriteria(Login.class);  
		      criteria.add( Restrictions.eq("loginName", loginName));
		      List results = criteria.list();
		      login =(Login) results.get(0);
		    } catch (Exception e) {
		    	System.out.print(e);
		      //JOptionPane.showMessageDialog(null, e.getMessage(), ������� 'findById'�, JOptionPane.OK_OPTION);
		    } finally {
		      if (session != null && session.isOpen()) {
		        session.close();
		      }
		    }
		    return login;
	  }


}
