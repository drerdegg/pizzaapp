package com.vsevolod.soroka.dao.mysql;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.hibernate.Session;

import com.vsevolod.soroka.HibernateUtil;
import com.vsevolod.soroka.dao.interfaces.ICustomer;
import com.vsevolod.soroka.entity.Courier;
import com.vsevolod.soroka.entity.Customer;

public class DAOCastomer implements ICustomer {

	public void addCustomer(Customer customer) throws SQLException {
		Session session = null;
	    try {
	      session = HibernateUtil.getSessionFactory().openSession();
	      session.beginTransaction();
	      session.save(customer);
	      session.getTransaction().commit();
	    } catch (Exception e) {
	      //JOptionPane.showMessageDialog(null, e.getMessage(), ������� ��� �������, JOptionPane.OK_OPTION);
	    } finally {
	      if (session != null && session.isOpen()) {

	        session.close();
	      }
	    }
	}

	public void updateCustomer(Customer customer) throws SQLException {
		Session session = null;
	    try {
	      session = HibernateUtil.getSessionFactory().openSession();
	      session.beginTransaction();
	      session.update(customer);
	      session.getTransaction().commit();
	    } catch (Exception e) {
	      //JOptionPane.showMessageDialog(null, e.getMessage(), ������� ��� �������, JOptionPane.OK_OPTION);
	    } finally {
	      if (session != null && session.isOpen()) {
	        session.close();
	      }
	    }
		
	}

	public void deleteCustomer(Customer customer) throws SQLException {
		Session session = null;
	    try {
	      session = HibernateUtil.getSessionFactory().openSession();
	      session.beginTransaction();
	      session.delete(customer);
	      session.getTransaction().commit();
	    } catch (Exception e) {
	      //JOptionPane.showMessageDialog(null, e.getMessage(), ������� ��� ��������, JOptionPane.OK_OPTION);
	    } finally {
	      if (session != null && session.isOpen()) {
	        session.close();
	      }
	    }
		
	}

	public Customer getCustomerByID(int customerID) throws SQLException {
		Session session = null;
	    Customer customer = null;
	    try {
	      session = HibernateUtil.getSessionFactory().openSession();
	      customer = (Customer) session.load(Customer.class, customerID);
	    } catch (Exception e) {
	      //JOptionPane.showMessageDialog(null, e.getMessage(), ������� 'findById'�, JOptionPane.OK_OPTION);
	    } finally {
	      if (session != null && session.isOpen()) {
	        session.close();
	      }
	    }
	    return customer;
	}

	public Collection getAllCustomer() throws SQLException {
		Session session = null;
	    List customer = new ArrayList<Customer>();
	    try {
	      session = HibernateUtil.getSessionFactory().openSession();
	      customer = session.createCriteria(Courier.class).list();
	    } catch (Exception e) {
	      //JOptionPane.showMessageDialog(null, e.getMessage(), ������� 'getAll'�, JOptionPane.OK_OPTION);
	    } finally {
	      if (session != null && session.isOpen()) {
	        session.close();
	      }
	    }
	    return customer;
	}

}
