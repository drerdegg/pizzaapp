package com.vsevolod.soroka.dao.mysql;

import java.sql.SQLException;
import java.util.ArrayList;

import org.hibernate.Session;

import com.vsevolod.soroka.HibernateUtil;
import com.vsevolod.soroka.dao.interfaces.IIngredientName;
import com.vsevolod.soroka.entity.IngredientName;

public class DAOIngredientName implements IIngredientName {

	@Override
	public void addIngredientName(IngredientName ingredientName)	throws SQLException {
		Session session = null;
	    try {
	      session = HibernateUtil.getSessionFactory().openSession();
	      session.beginTransaction();
	      session.save(ingredientName);
	      session.getTransaction().commit();
	    } catch (Exception e) {
	      System.out.print(e);//JOptionPane.showMessageDialog(null, e.getMessage(), ������� ��� �������, JOptionPane.OK_OPTION);
	    } finally {
	      if (session != null && session.isOpen()) {

	        session.close();
	      }
	    }
	}

	@Override
	public void updateIngredientName(IngredientName ingredientName)
			throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteIngredientName(IngredientName ingredientName)
			throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public IngredientName getIngredientNameByID(int ingredientNameID)
			throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<IngredientName> getAllIngredientName() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

}
