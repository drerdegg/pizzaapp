package com.vsevolod.soroka.dao.mysql;

import java.sql.SQLException;
import java.util.Collection;

import org.hibernate.Session;

import com.vsevolod.soroka.HibernateUtil;
import com.vsevolod.soroka.dao.interfaces.IOrder;
import com.vsevolod.soroka.entity.Order;
import com.vsevolod.soroka.entity.Pizza;

public class DAOOrder implements IOrder {

	@Override
	public void addOrder(Order order) throws SQLException {
		Session session = null;
	    try {
	      session = HibernateUtil.getSessionFactory().openSession();
	      session.beginTransaction();
	      session.save(order);
	      session.getTransaction().commit();
	    } catch (Exception e) {
	    	System.out.print(e);
	      //JOptionPane.showMessageDialog(null, e.getMessage(), ������� ��� �������, JOptionPane.OK_OPTION);
	    } finally {
	      if (session != null && session.isOpen()) {

	        session.close();
	      }
	    }
	}

	@Override
	public void updateOrder(Order order) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteOrder(Order order) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public Order getOrderByID(int orderID) throws SQLException {
		Session session = null;
	    Order order = null;
	    try {
	      session = HibernateUtil.getSessionFactory().openSession();
	      order = (Order) session.get(Order.class, orderID);
	    } catch (Exception e) {
	    	System.out.print(e);
	      //JOptionPane.showMessageDialog(null, e.getMessage(), ������� 'findById'�, JOptionPane.OK_OPTION);
	    } finally {
	      if (session != null && session.isOpen()) {
	        //session.close();
	      }
	    }
	    return order;
	}

	@Override
	public Collection getAllOrder() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

}
