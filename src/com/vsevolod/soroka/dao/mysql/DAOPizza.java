package com.vsevolod.soroka.dao.mysql;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.hibernate.Session;

import com.vsevolod.soroka.HibernateUtil;
import com.vsevolod.soroka.dao.factory.Factory;
import com.vsevolod.soroka.dao.interfaces.IPizza;
import com.vsevolod.soroka.entity.Courier;
import com.vsevolod.soroka.entity.Ingredient;
import com.vsevolod.soroka.entity.Login;
import com.vsevolod.soroka.entity.Pizza;

public class DAOPizza implements IPizza {

	@Override
	public void addPizza(Pizza pizza) throws SQLException {
		Session session = null;
		String stringIngredientsID = "|";
	    try {
	      session = HibernateUtil.getSessionFactory().openSession();
	      session.beginTransaction();
//	      for (Integer i : pizza.getIngredientsId()) {
//			 stringIngredientsID += Factory.getInstance().getIngredientDAO().addIngredient(i) + "|";
//	      }
//	      pizza.setIngredientsID(stringIngredientsID);
	      session.save(pizza);
	      session.getTransaction().commit();
	    } catch (Exception e) {
	      //JOptionPane.showMessageDialog(null, e.getMessage(), ������� ��� �������, JOptionPane.OK_OPTION);
	    } finally {
	      if (session != null && session.isOpen()) {

	        session.close();
	      }
	    }
		
	}

	@Override
	public void updatePizza(Pizza pizza) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void deletePizza(Pizza pizza) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public Pizza getPizzaByID(int pizzaID) throws SQLException {
		Session session = null;
	    Pizza pizza = null;
	    try {
	      session = HibernateUtil.getSessionFactory().openSession();
	      pizza = (Pizza) session.get(Pizza.class, pizzaID);
	    } catch (Exception e) {
	    	System.out.print(e);
	      //JOptionPane.showMessageDialog(null, e.getMessage(), ������� 'findById'�, JOptionPane.OK_OPTION);
	    } finally {
	      if (session != null && session.isOpen()) {
	        //session.close();
	      }
	    }
	    return pizza;
	}

	@Override
	public ArrayList<Pizza> getAllPizza() throws SQLException {
		Session session = null;
	    List pizzas = new ArrayList<Courier>();
	    try {
	      session = HibernateUtil.getSessionFactory().openSession();
	      pizzas = session.createCriteria(Pizza.class).list();
	    } catch (Exception e) {
	      //JOptionPane.showMessageDialog(null, e.getMessage(), ������� 'getAll'�, JOptionPane.OK_OPTION);
	    } finally {
	      if (session != null && session.isOpen()) {
	        session.close();
	      }
	    }
	    return (ArrayList<Pizza>)pizzas;
	}

}
