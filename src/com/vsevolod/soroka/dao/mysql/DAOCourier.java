package com.vsevolod.soroka.dao.mysql;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.hibernate.Session;

import com.vsevolod.soroka.HibernateUtil;
import com.vsevolod.soroka.dao.interfaces.ICourier;
import com.vsevolod.soroka.entity.Courier;

public class DAOCourier implements ICourier {

	public void addCourier(Courier courier) throws SQLException {
		Session session = null;
	    try {
	      session = HibernateUtil.getSessionFactory().openSession();
	      session.beginTransaction();
	      session.save(courier);
	      session.getTransaction().commit();
	    } catch (Exception e) {
	      //JOptionPane.showMessageDialog(null, e.getMessage(), ������� ��� �������, JOptionPane.OK_OPTION);
	    } finally {
	      if (session != null && session.isOpen()) {

	        session.close();
	      }
	    }
		
	}

	public void updateCourier(Courier courier) throws SQLException {
		Session session = null;
	    try {
	      session = HibernateUtil.getSessionFactory().openSession();
	      session.beginTransaction();
	      session.update(courier);
	      session.getTransaction().commit();
	    } catch (Exception e) {
	      //JOptionPane.showMessageDialog(null, e.getMessage(), ������� ��� �������, JOptionPane.OK_OPTION);
	    } finally {
	      if (session != null && session.isOpen()) {
	        session.close();
	      }
	    }
	}

	public void deleteCourier(Courier courier) throws SQLException {
		Session session = null;
	    try {
	      session = HibernateUtil.getSessionFactory().openSession();
	      session.beginTransaction();
	      session.delete(courier);
	      session.getTransaction().commit();
	    } catch (Exception e) {
	      //JOptionPane.showMessageDialog(null, e.getMessage(), ������� ��� ��������, JOptionPane.OK_OPTION);
	    } finally {
	      if (session != null && session.isOpen()) {
	        session.close();
	      }
	    }
	}

	public Courier getCourierByID(int courierID) throws SQLException {
		Session session = null;
	    Courier courier = null;
	    try {
	      session = HibernateUtil.getSessionFactory().openSession();
	      courier = (Courier) session.load(Courier.class, courierID);
	    } catch (Exception e) {
	      //JOptionPane.showMessageDialog(null, e.getMessage(), ������� 'findById'�, JOptionPane.OK_OPTION);
	    } finally {
	      if (session != null && session.isOpen()) {
	        session.close();
	      }
	    }
	    return courier;
	}

	@Override
	public ArrayList<Courier> getAllCourier() throws SQLException {
		Session session = null;
	    List couriers = new ArrayList<Courier>();
	    try {
	      session = HibernateUtil.getSessionFactory().openSession();
	      couriers = session.createCriteria(Courier.class).list();
	    } catch (Exception e) {
	      //JOptionPane.showMessageDialog(null, e.getMessage(), ������� 'getAll'�, JOptionPane.OK_OPTION);
	    } finally {
	      if (session != null && session.isOpen()) {
	        session.close();
	      }
	    }
	    return (ArrayList<Courier>)couriers;
	}

}
