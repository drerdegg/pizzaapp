package com.vsevolod.soroka.dao.mysql;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import com.vsevolod.soroka.HibernateUtil;
import com.vsevolod.soroka.dao.interfaces.IIngredient;
import com.vsevolod.soroka.entity.Courier;
import com.vsevolod.soroka.entity.Ingredient;

public class DAOIngredient implements IIngredient {

	@Override
	public void addIngredient(Ingredient ingredient) throws SQLException {
		Session session = null;
	    try {
	      session = HibernateUtil.getSessionFactory().openSession();
	      session.beginTransaction();
	      session.save(ingredient);
	      session.getTransaction().commit();
	    } catch (Exception e) {
	      System.out.print(e);//JOptionPane.showMessageDialog(null, e.getMessage(), ������� ��� �������, JOptionPane.OK_OPTION);
	    } finally {
	      if (session != null && session.isOpen()) {

	        session.close();
	      }
	    }
	}

	@Override
	public void updateIngredient(Ingredient ingredient) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteIngredient(Ingredient ingredient) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public Ingredient getIngredientByID(int ingredientsID) throws SQLException {
		Session session = null;
		Ingredient ingredient = null;
	    try {
	      session = HibernateUtil.getSessionFactory().openSession();
	      ingredient = (Ingredient) session.load(Ingredient.class, ingredientsID);
	    } catch (Exception e) {
	    	System.out.print(e);
	      //JOptionPane.showMessageDialog(null, e.getMessage(), ������� 'findById'�, JOptionPane.OK_OPTION);
	    } finally {
	      if (session != null && session.isOpen()) {
	        //session.close();
	      }
	    }
	    return ingredient;
	}

	@Override
	public ArrayList<Ingredient> getAllIngredient() throws SQLException {
		Session session = null;
	    List ingredients = new ArrayList<Ingredient>();
	    try {
	      session = HibernateUtil.getSessionFactory().openSession();
	      ingredients =  session.createCriteria(Ingredient.class).list();
	    } catch (Exception e) {
	      //JOptionPane.showMessageDialog(null, e.getMessage(), ������� 'getAll'�, JOptionPane.OK_OPTION);
	    } finally {
	      if (session != null && session.isOpen()) {
	        //session.close();
	      }
	    }
	    return (ArrayList<Ingredient>)ingredients;
	}

}
