package com.vsevolod.soroka.dao.factory;

import com.vsevolod.soroka.dao.interfaces.*;
import com.vsevolod.soroka.dao.mysql.*;

public class Factory {
	private static DAOCourier courierDAO = null;
	private static DAOLogin loginDAO = null;
	private static Factory instance = null;
	private static DAOIngredient ingredientDAO = null;
	private static DAOIngredientName  ingredientNameDAO = null;
	private static DAOPizza pizzaDAO = null;
	private static DAOOrder orderDAO = null;
	
	  public static synchronized Factory getInstance(){
	    if (instance == null){
	      instance = new Factory();
	    }
	    return instance;
	  }

	  public ICourier getCourierDAO(){
	    if (courierDAO == null){
	      courierDAO = new DAOCourier();
	    }
	    return courierDAO;
	  }
	  
	  public ILogin getLoginDAO(){
		    if (loginDAO == null){
		       loginDAO = new DAOLogin();
		    }
		    return loginDAO;
		  }
	  
	  public IIngredient getIngredientDAO(){
		    if (ingredientDAO == null){
		    	ingredientDAO = new DAOIngredient();
		    }
		    return ingredientDAO;
		  }
	  
	  public IPizza getPizzaDAO(){
		    if (pizzaDAO == null){
		    	pizzaDAO = new DAOPizza();
		    }
		    return pizzaDAO;
		  }
	
	  public IIngredientName getIngredientNameDAO(){
		    if (ingredientNameDAO == null){
		    	ingredientNameDAO = new DAOIngredientName();
		    }
		    return ingredientNameDAO;
		  }
	  
	  public IOrder getOrderDAO(){
		    if (orderDAO == null){
		    	orderDAO = new DAOOrder();
		    }
		    return orderDAO;
		  }
	  
}
