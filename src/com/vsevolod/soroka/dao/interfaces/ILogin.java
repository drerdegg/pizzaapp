package com.vsevolod.soroka.dao.interfaces;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import com.vsevolod.soroka.entity.Login;

public interface ILogin {
	public void addLogin(Login login)throws SQLException;
	public void updateLogin(Login login)throws SQLException;
	public void deleteLogin(Login login)throws SQLException;
	public Login getLoginByID(int loginID)throws SQLException;
	public ArrayList<Login> getAllLogin() throws SQLException; 
	public Login getLoginByLoginsName(String loginName) throws SQLException; 
	
}
