package com.vsevolod.soroka.dao.interfaces;

import java.sql.SQLException;
import java.util.Collection;

import com.vsevolod.soroka.entity.Customer;

public interface ICustomer {
	public void addCustomer(Customer customer)throws SQLException;
	public void updateCustomer(Customer customer)throws SQLException;
	public void deleteCustomer(Customer customer)throws SQLException;
	public Customer getCustomerByID(int customerID)throws SQLException;
	public Collection getAllCustomer() throws SQLException; 
}
