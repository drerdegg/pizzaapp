package com.vsevolod.soroka.dao.interfaces;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import com.vsevolod.soroka.entity.Courier;


public interface ICourier {
	public void addCourier(Courier courier)throws SQLException;
	public void updateCourier(Courier courier)throws SQLException;
	public void deleteCourier(Courier courier)throws SQLException;
	public Courier getCourierByID(int courierID)throws SQLException;
	public ArrayList<Courier> getAllCourier() throws SQLException; 
	
}
