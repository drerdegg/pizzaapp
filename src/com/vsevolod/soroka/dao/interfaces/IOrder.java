package com.vsevolod.soroka.dao.interfaces;

import java.sql.SQLException;
import java.util.Collection;

import com.vsevolod.soroka.entity.Order;

public interface IOrder {
	public void addOrder(Order order)throws SQLException;
	public void updateOrder(Order order)throws SQLException;
	public void deleteOrder(Order order)throws SQLException;
	public Order getOrderByID(int orderID)throws SQLException;
	public Collection getAllOrder() throws SQLException; 
}
