package com.vsevolod.soroka.dao.interfaces;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import com.vsevolod.soroka.entity.Pizza;

public interface IPizza {
	public void addPizza(Pizza pizza)throws SQLException;
	public void updatePizza(Pizza pizza)throws SQLException;
	public void deletePizza(Pizza pizza)throws SQLException;
	public Pizza getPizzaByID(int pizzaID)throws SQLException;
	public ArrayList<Pizza> getAllPizza() throws SQLException; 
}
