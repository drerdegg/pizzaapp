package com.vsevolod.soroka.dao.interfaces;

import java.sql.SQLException;
import java.util.ArrayList;

import com.vsevolod.soroka.entity.IngredientName;

public interface IIngredientName {
	public void addIngredientName(IngredientName ingredientName)throws SQLException;
	public void updateIngredientName(IngredientName ingredientName)throws SQLException;
	public void deleteIngredientName(IngredientName ingredientName)throws SQLException;
	public IngredientName getIngredientNameByID(int ingredientNameID)throws SQLException;
	public ArrayList<IngredientName> getAllIngredientName() throws SQLException; 
}
