package com.vsevolod.soroka.dao.interfaces;

import java.sql.SQLException;
import java.util.ArrayList;

import com.vsevolod.soroka.entity.Ingredient;

public interface IIngredient {
	public void addIngredient(Ingredient ingredient)throws SQLException;
	public void updateIngredient(Ingredient ingredient)throws SQLException;
	public void deleteIngredient(Ingredient ingredient)throws SQLException;
	public Ingredient getIngredientByID(int ingredientID)throws SQLException;
	public ArrayList<Ingredient> getAllIngredient() throws SQLException; 

}
