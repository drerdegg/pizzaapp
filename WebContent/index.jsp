<%@ taglib uri="/WEB-INF/tlds/footertag.tld" prefix="footertag" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<fmt:setLocale value="en"/> 
<fmt:setBundle basename="com.vsevolod.soroka.localization.data" var="bundle"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
                <form class="form-container" name="goToRegistratePage" method="POST" action="controller">
            <input type="hidden" name="command" value="goToRegistratePage"/>
                <div class="submit-container">
                    <input class="submit-button" type="submit" value="<fmt:message key="main.button.registration" bundle="${bundle}"/>" />
                </div>
            </form>
            
            <form class="form-container" name="goToLoginPage" method="POST" action="controller">
            <input type="hidden" name="command" value="goToLoginPage"/>
                <div class="submit-container">
                    <input class="submit-button" type="submit" value="<fmt:message key="main.button.login" bundle="${bundle}"/>" />
                </div>
            </form>
</body>
</html>