<%-- 
    Document   : login
    Created on : 18.04.2014, 17:49:35
    Author     : saresh
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="/WEB-INF/tlds/footertag.tld" prefix="footertag" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <fmt:setLocale value="en"/> 
    <fmt:setBundle basename="com.vsevolod.soroka.localization.data" var="bundle"/>

    <body style="text-align: center;">
        <div style="margin:0 auto; width: 342px; text-align: center;">
            <form class="form-container" name="loginForm" method="POST" action="controller">
                <input type="hidden" name="command" value="login">
                <div class="form-title"><h2><fmt:message key="login.title" bundle="${bundle}"/></h2></div>
                <div class="form-title"><fmt:message key="login.login.label" bundle="${bundle}"/></div>
                <input class="form-field" type="text" name="login" /><br />
                <div class="form-title"><fmt:message key="login.password.label" bundle="${bundle}"/></div>
                <input class="form-field" type="password" name="password" /><br />
                <div class="submit-container">
                    <input class="submit-button" type="submit" value="<fmt:message key="login.button.label" bundle="${bundle}"/>" />
                </div>
            </form>
            
        </div> 
        <footertag:print/>

    </body>
</html>
