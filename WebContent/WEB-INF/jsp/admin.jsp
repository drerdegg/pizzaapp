<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="/WEB-INF/tlds/footertag.tld" prefix="footertag" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <fmt:setLocale value="en"/> 
    <fmt:setBundle basename="com.vsevolod.soroka.localization.data" var="bundle"/>
<body>
	admin
	 		<form class="form-container" name="goToPizzaWorker" method="POST" action="controller">
            <input type="hidden" name="command" value="goToPizzaWorker"/>
                <div class="submit-container">
                    <input class="submit-button" type="submit" value="<fmt:message key="admin.button.pizza" bundle="${bundle}"/>" />
                </div>
            </form>
            
            <form class="form-container" name="goToLoginWorker" method="POST" action="controller">
            <input type="hidden" name="command" value="goToLoginWorker"/>
                <div class="submit-container">
                    <input class="submit-button" type="submit" value="<fmt:message key="admin.button.login" bundle="${bundle}"/>" />
                </div>
            </form>
            
            <form class="form-container" name="goToCourierWorker" method="POST" action="controller">
            <input type="hidden" name="command" value="goToCourierWorker"/>
                <div class="submit-container">
                    <input class="submit-button" type="submit" value="<fmt:message key="admin.button.courier" bundle="${bundle}"/>" />
                </div>
            </form>
             <form class="form-container" name="goToIngredientWorker" method="POST" action="controller">
            <input type="hidden" name="command" value="goToIngredientWorker"/>
                <div class="submit-container">
                    <input class="submit-button" type="submit" value="<fmt:message key="admin.button.ingredient" bundle="${bundle}"/>" />
                </div>
            </form>
</body>
</html>