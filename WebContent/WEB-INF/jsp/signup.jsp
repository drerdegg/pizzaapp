<%-- 
    Document   : signup
    Created on : 23.04.2014, 15:47:55
    Author     : saresh
--%>
<%@ taglib uri="/WEB-INF/tlds/footertag.tld" prefix="footertag" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <fmt:setLocale value="en"/> 
    <fmt:setBundle basename="com.vsevolod.soroka.localization.data" var="bundle"/>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><fmt:message key="signup.page.title" bundle="${bundle}"/></title>      
        <link rel="stylesheet" href="css/style.css" type="text/css">
    </head>
    <body>
       
        <div style="margin:0 auto; width: 342px; text-align: center; padding-bottom: 70px;">
            <form action="controller" method="POST" name="signUp" class="form-container">
                <input type="hidden" name="command" value="signUp"/>
                <div class="form-title"><fmt:message key="signup.login.label" bundle="${bundle}"/></div><input type="text" name="login" class="form-field"/>
                <div class="form-title"><fmt:message key="signup.password.label" bundle="${bundle}"/></div><input type="password" name="password" class="form-field"/>
                <div class="form-title"><fmt:message key="signup.fname.label" bundle="${bundle}"/></div> <input type="text" name="firstName" class="form-field"/>
                <div class="form-title"><fmt:message key="signup.lname.label" bundle="${bundle}"/></div> <input type="text" name="lastName" class="form-field"/>
                <div class="form-title"><fmt:message key="signup.email.label" bundle="${bundle}"/></div> <input type="text" name="email" class="form-field"/>
                <div class="form-title"><fmt:message key="signup.telephone.label" bundle="${bundle}"/></div> <input type="text" name="phoneNumber" class="form-field"/>
                <div class="form-title"><fmt:message key="signup.adress.label" bundle="${bundle}"/></div> <input type="text" name="adress" class="form-field"/>
                <div class="submit-container">
                    <input type="submit" class="submit-button" value="<fmt:message key="signup.button.label" bundle="${bundle}"/>"/>
                </div>
            </form>
        </div>
        <footertag:print/>
    </body>
</html>
