
<%@page import="java.util.ArrayList"%>
<%@page import="com.vsevolod.soroka.entity.Courier"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="/WEB-INF/tlds/footertag.tld" prefix="footertag" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<fmt:setLocale value="en"/> 
    <fmt:setBundle basename="com.vsevolod.soroka.localization.data" var="bundle"/>

<body>
Courier Worker
 <div style="margin:0 auto; width: 342px; text-align: center;">
            <form class="form-container" name="addCourier" method="POST" action="controller">
             <input type="hidden" name="command" value="addCourier" />
                <div class="form-title"><h2><fmt:message key="courierWorker.tittle.addCourrier" bundle="${bundle}"/></h2></div>
                <div class="form-title"><fmt:message key="courierWorker.label.firstName" bundle="${bundle}"/></div>
                <input class="form-field" type="text" name="firstName" /><br />
                <div class="form-title"><fmt:message key="courierWorker.label.lastName" bundle="${bundle}"/></div>
                <input class="form-field" type="text" name="lastName" /><br />
                <div class="form-title"><fmt:message key="courierWorker.label.phoneNumber" bundle="${bundle}"/></div>
                <input class="form-field" type="text" name="phoneNumber" /><br />
                <div class="submit-container">
                    <input class="submit-button" type="submit" value="<fmt:message key="courierWorker.button.add" bundle="${bundle}"/>" />
                </div>
            </form>
            
        </div>
<%ArrayList<Courier> result = (ArrayList<Courier>)session.getAttribute("AllCouriers");
if(result!=null){ 
for (Courier c : result){  %>
<input type="submit" name="<%=c.getId()%>" value="<%="btn"%>"> <%=c%><br>
<% }}%>
</body>
</html>